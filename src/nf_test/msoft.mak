# nmake Makefile for netCDF (semi)exhaustive FORTRAN test.
#
# $Id: Makefile,v 1.4 1997/05/15 20:12:41 steve Exp $

# Release
# COPT= /nologo /W1 /FD /MT          /O2
# /W3 generates many warnings, all of which should be okay...
COPT=   /nologo /W1 /FD /MTd /Gm /Zi /Od /D "_DEBUG"
INCLUDES= /I..\libsrc
DEFINES= /D "NDEBUG" /D "DLL_NETCDF" /D "VISUAL_CPLUSPLUS"
EXPORT= /D "DLL_EXPORT" /D FCALLSC_QUALIFIER="__declspec(dllexport) __stdcall"
CFLAGS= $(COPT) $(INCLUDES) $(DEFINES)
FOR=df
FFLAGS= /nologo /Zi /Od
#FFLAGS= /nologo /debug:full /optimize:0

PATH = ..\ncdump;..\libsrc;$(PATH)

#
FOBJS		= \
		  test_get.obj	\
		  test_put.obj	\
		  nf_error.obj	\
		  nf_test.obj	\
		  test_read.obj	\
		  test_write.obj \
		  util.obj

OBJS		=  $(FOBJS) fortlib.obj

LIBRARY		= ..\libsrc\netcdf.lib

all:		nf_test.exe

test:		nf_test.exe test.nc
	.\nf_test.exe

readonly:	nf_test.exe test.nc
	.\nf_test -r

test.nc:  nf_test.exe
	.\nf_test -c

dump:
	ncdump test.nc

install:

nf_test.exe: $(OBJS) $(LIBRARY)
	$(FOR) $(FFLAGS) $(OBJS) /link $(LIBRARY)  /NODEFAULTLIB:LIBCMTD /out:$@

conftest.exe: conftest.obj conftestf.obj
	$(FOR) $(FFLAGS) conftestf.obj conftest.obj /link /NODEFAULTLIB:LIBCMTD /out:$@

clean:
	-@erase *.obj *.for nf_test.exe test.nc > NUL 2>&1
	-@erase *.pdb *.ilk *.idb *.rsp > NUL 2>&1

distclean: clean

#depend
fortlib.obj: fortlib.c

nf_error.obj: nf_error.for
	$(FOR) /c $(FFLAGS) $*.for
nf_test.obj: nf_test.for
	$(FOR) /c $(FFLAGS) $*.for
test_get.obj: test_get.for
	$(FOR) /c $(FFLAGS) $*.for
test_put.obj: test_put.for
	$(FOR) /c $(FFLAGS) $*.for
test_read.obj: test_read.for
	$(FOR) /c $(FFLAGS) $*.for
test_write.obj: test_write.for
	$(FOR) /c $(FFLAGS) $*.for
util.obj: util.for
	$(FOR) /c $(FFLAGS) $*.for

nf_error.for: nf_error.F
	$(CC) /nologo /EP /C $(INCLUDES) $(DEFINES) $? > $@
nf_test.for: nf_test.F
	$(CC) /nologo /EP /C $(INCLUDES) $(DEFINES) $? > $@
test_get.for: test_get.F
	$(CC) /nologo /EP /C $(INCLUDES) $(DEFINES) $? > $@
test_put.for: test_put.F
	$(CC) /nologo /EP /C $(INCLUDES) $(DEFINES) $? > $@
test_read.for: test_read.F
	$(CC) /nologo /EP /C $(INCLUDES) $(DEFINES) $? > $@
test_write.for: test_write.F
	$(CC) /nologo /EP /C $(INCLUDES) $(DEFINES) $? > $@
util.for: util.F
	$(CC) /nologo /EP /C $(INCLUDES) $(DEFINES) $? > $@

