# nmake Makefile for netCDF exhaustive test.
# static library version
# JCaron 3/18/98

#COPT= /nologo /MT /W3 /GX /O2 /YX
COPT=   /nologo /W1 /FD /MTd /Gm /Zi /Od /D "_DEBUG"
#DEFINES = /D "NDEBUG"
INCLUDES= /I..\libsrc
DEFINES= $(DEFINES) /D "WIN32" /D "_WINDOWS" /D "VISUAL_CPLUSPLUS"
CFLAGS= $(COPT) $(INCLUDES) $(DEFINES)
LFLAGS = /NODEFAULTLIB:libcmt /NODEFAULTLIB:libcmtd 

FOR=df
FFLAGS= /nologo /Zi /Od
#FFLAGS= /nologo /debug:full /optimize:0

PATH = ..\ncdump;$(PATH)

#
FOBJS		= \
		  test_get.obj	\
		  test_put.obj	\
		  nf_error.obj	\
		  nf_test.obj	\
		  test_read.obj	\
		  test_write.obj \
		  util.obj

OBJS		=  $(FOBJS) fortlib.obj

LIBRARY		= ..\libsrc\netcdfs.lib

all:		nf_test.exe

test:		nf_test.exe test.nc
	.\nf_test.exe

readonly:	nf_test.exe test.nc
	.\nf_test -r

test.nc:  nf_test.exe
	.\nf_test -c

dump:
	ncdump test.nc

install:

nf_test.exe: $(OBJS) $(LIBRARY)
	$(FOR) $(FFLAGS) $(OBJS) /link $(LIBRARY) $(LFLAGS) /out:$@

conftest.exe: conftest.obj conftestf.obj
	$(FOR) $(FFLAGS) conftestf.obj conftest.obj /link $(LFLAGS) /out:$@

clean:
	-@erase *.obj *.for nf_test.exe test.nc > NUL 2>&1
	-@erase *.pdb *.ilk *.idb *.rsp > NUL 2>&1

distclean: clean

#depend
fortlib.obj: fortlib.c

nf_error.obj: nf_error.for
	$(FOR) /c $(FFLAGS) $*.for
nf_test.obj: nf_test.for
	$(FOR) /c $(FFLAGS) $*.for
test_get.obj: test_get.for
	$(FOR) /c $(FFLAGS) $*.for
test_put.obj: test_put.for
	$(FOR) /c $(FFLAGS) $*.for
test_read.obj: test_read.for
	$(FOR) /c $(FFLAGS) $*.for
test_write.obj: test_write.for
	$(FOR) /c $(FFLAGS) $*.for
util.obj: util.for
	$(FOR) /c $(FFLAGS) $*.for

nf_error.for: nf_error.F
	$(CC) /nologo /EP /C $(INCLUDES) $(DEFINES) $? > $@
nf_test.for: nf_test.F
	$(CC) /nologo /EP /C $(INCLUDES) $(DEFINES) $? > $@
test_get.for: test_get.F
	$(CC) /nologo /EP /C $(INCLUDES) $(DEFINES) $? > $@
test_put.for: test_put.F
	$(CC) /nologo /EP /C $(INCLUDES) $(DEFINES) $? > $@
test_read.for: test_read.F
	$(CC) /nologo /EP /C $(INCLUDES) $(DEFINES) $? > $@
test_write.for: test_write.F
	$(CC) /nologo /EP /C $(INCLUDES) $(DEFINES) $? > $@
util.for: util.F
	$(CC) /nologo /EP /C $(INCLUDES) $(DEFINES) $? > $@

