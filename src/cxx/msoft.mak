#
#	nmake Makefile for netcdf c++ interface
#
!IF "$(CFG)" == ""
CFG=Release
!ENDIF 
!IF "$(CFG)" != "Debug" && "$(CFG)" != "Release"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "msoft.mak" CFG="Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Release"
!MESSAGE "Debug"
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF  "$(CFG)" == "Debug"
COPT=   /nologo /W3 /FD /MTd /Gm /Zi /Od /D "_DEBUG"
!ELSE 
# Release
COPT= /nologo /W3 /FD /MT /O2
!ENDIF 


DEFINES= /D "NDEBUG" /D "DLL_NETCDF" /D "_WINDOWS"
EXPORT= /D "DLL_EXPORT" 
INCLUDES	= -I../libsrc -I.
CPPFLAGS= $(COPT) $(INCLUDES) $(DEFINES) $(EXPORT)


DLL		= netcdfpp.dll
LIBRARY	= netcdfpp.lib
HEADER 	= netcdf.h
NETCDFLIB	= ..\libsrc\netcdf.lib

LIB_OBJS	= netcdf.obj ncvalues.obj

GARBAGE	= $(prog) test.out example.nc *.obj \
		  *.cps *.dvi *.fns \
		  *.log *~ *.gs *.aux *.cp *.fn *.ky *.pg *.toc *.tp *.vr

all: $(DLL)

check: test

install:	$(LIBDIR)/$(LIBRARY) $(INCDIR)/$(HEADER1) $(INCDIR)/$(HEADER2)\
		$(INCDIR)/$(HEADER3)


LINK32=link.exe
LINK32_FLAGS= /nologo /dll /incremental:no /DEFAULTLIB:$(NETCDFLIB) 

$(DLL) $(LIBRARY): $(LIB_OBJS)
    $(LINK32) $(LINK32_FLAGS) /out:$(DLL) /implib:$(LIBRARY) $(LIB_OBJS) /FORCE:UNRESOLVED

#################
# testing

prog = nctst.exe
prog_objs 	= nctst.obj
libs = $(LIBRARY) $(NETCDFLIB)

test:	 $(prog) FORCE
	./$(prog) > test.out
	@cmp expected test.out && \
	    echo "*** C++ test successful ***" ;


$(prog) : $(prog_objs)
	$(CXX) -o $@ $(CXXFLAGS) $(LDFLAGS) $(prog_objs) $(libs)

clean:
	-@erase *.obj t_nc.exe test.nc > NUL 2>&1

distclean: clean
	-@erase $(GARBAGE) > NUL 2>&1
	-@erase *.pdb *.ilk *.idb *.rsp > NUL 2>&1
	-@erase *.dll *.lib > NUL 2>&1

	
	
# depends
netcdf.o: netcdfcpp.h ../libsrc/netcdf.h netcdf.cpp

ncvalues.o: ncvalues.h ncvalues.cpp

nctst.o: netcdfcpp.h nctst.cpp