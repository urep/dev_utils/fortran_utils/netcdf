#
#	nmake Makefile for netcdf c++ interface static library
#
!IF "$(CFG)" == ""
CFG=Release
!ENDIF 
!IF "$(CFG)" != "Debug" && "$(CFG)" != "Release"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "msoft.mak" CFG="Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Release"
!MESSAGE "Debug"
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF  "$(CFG)" == "Debug"
COPT= /nologo /MTd /W3 /Gm /GX /Zi /Od /YX 
DEFINES= /D "WIN32" /D "_DEBUG" /D "_WINDOWS"
!ELSE 
# Release
COPT= /nologo /MT /W3 /GX /O2 /YX
DEFINES = /D "WIN32" /D "NDEBUG" /D "_WINDOWS"
!ENDIF 

INCLUDES	= -I../libsrc -I.
CPPFLAGS= $(COPT) $(INCLUDES) $(DEFINES)

LIBRARY	= netcdfpps.lib
HEADER 	= netcdf.h
NETCDFLIB	= ..\libsrc\netcdfs.lib

LIB_OBJS	= netcdf.obj ncvalues.obj

GARBAGE	= $(prog) test.out example.nc *.obj \
		  *.cps *.dvi *.fns \
		  *.log *~ *.gs *.aux *.cp *.fn *.ky *.pg *.toc *.tp *.vr

all: $(LIBRARY)

check: test

install:	$(LIBDIR)/$(LIBRARY) $(INCDIR)/$(HEADER1) $(INCDIR)/$(HEADER2)\
		$(INCDIR)/$(HEADER3)


LINK32=link.exe
LINK32_FLAGS= /nologo /dll /incremental:no /DEFAULTLIB:$(NETCDFLIB)
LIBMAKE=lib.exe

$(LIBRARY): $(LIB_OBJS)
	$(LIBMAKE) /out:$(LIBRARY) $(LIB_OBJS)

#################
# testing

prog = nctst.exe
prog_objs 	= nctst.obj
libs = $(LIBRARY) $(NETCDFLIB)

test:	 $(prog) 
	.\$(prog) > test.out
	@cmp expected test.out && \
	    echo "*** C++ test successful ***" ;


$(prog) : $(prog_objs) $(LIBRARY) $(lib_netcdf)
	$(CXX) -o $@ $(CXXFLAGS) $(LDFLAGS) $(prog_objs) $(libs)

clean:
	-@erase *.obj t_nc.exe test.nc > NUL 2>&1

distclean: clean
	-@erase $(GARBAGE) > NUL 2>&1
	-@erase *.pdb *.ilk *.idb *.rsp > NUL 2>&1
	-@erase *.dll *.lib > NUL 2>&1

	
	
# depends
netcdf.obj: netcdfcpp.h ../libsrc/netcdf.h netcdf.cpp

ncvalues.obj: ncvalues.h ncvalues.cpp

nctst.obj: netcdfcpp.h nctst.cpp
