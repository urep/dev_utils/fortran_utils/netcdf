# nmake Makefile for netCDF (semi)exhaustive test.
#
# $Id: Makefile,v 1.10 1997/05/15 20:12:26 steve Exp $

# Release
# COPT= /nologo /W3 /FD /MT          /O2
COPT=   /nologo /W3 /FD /MTd /Gm /Zi /Od /D "_DEBUG"
INCLUDES= /I..\libsrc
DEFINES= /D "NDEBUG" /D "DLL_NETCDF"
CFLAGS= $(COPT) $(INCLUDES) $(DEFINES)

PATH = ..\libsrc;$(PATH)

SRCS		= varget.c vargetg.c varput.c varputg.c vardef.c vartests.c \
		  vputget.c vputgetg.c driver.c cdftests.c dimtests.c rec.c \
		  atttests.c misctest.c add.c error.c emalloc.c val.c slabs.c

OBJS		= $(SRCS:.c=.obj)

LIBRARY		= ..\libsrc\netcdf.lib

all:		nctest.exe

test:		nctest.exe
	@echo $(PATH)
	.\nctest.exe
	cmp testfile.nc testfile_nc.sav

install:

nctest.exe: $(OBJS) $(LIBRARY)
	$(CC) $(CFLAGS) $(OBJS) $(LIBRARY) /Fe$@

clean:
	-@erase *.obj nctest.exe testfile.nc test2.nc > NUL 2>&1
	-@erase *.pdb *.ilk *.idb *.rsp > NUL 2>&1

distclean: clean

#depend
add.obj: ..\libsrc\netcdf.h
add.obj: add.c
add.obj: add.h
add.obj: emalloc.h
add.obj: testcdf.h
atttests.obj: ..\libsrc\netcdf.h
atttests.obj: add.h
atttests.obj: atttests.c
atttests.obj: emalloc.h
atttests.obj: error.h
atttests.obj: testcdf.h
atttests.obj: tests.h
atttests.obj: val.h
cdftests.obj: ..\libsrc\netcdf.h
cdftests.obj: add.h
cdftests.obj: cdftests.c
cdftests.obj: emalloc.h
cdftests.obj: error.h
cdftests.obj: testcdf.h
cdftests.obj: tests.h
dimtests.obj: ..\libsrc\netcdf.h
dimtests.obj: add.h
dimtests.obj: dimtests.c
dimtests.obj: emalloc.h
dimtests.obj: error.h
dimtests.obj: testcdf.h
dimtests.obj: tests.h
driver.obj: ..\libsrc\netcdf.h
driver.obj: driver.c
driver.obj: tests.h
emalloc.obj: emalloc.c
emalloc.obj: emalloc.h
emalloc.obj: error.h
error.obj: ..\libsrc\netcdf.h
error.obj: error.c
error.obj: error.h
misctest.obj: ..\libsrc\netcdf.h
misctest.obj: add.h
misctest.obj: emalloc.h
misctest.obj: error.h
misctest.obj: misctest.c
misctest.obj: testcdf.h
nctime.obj: ..\libsrc\netcdf.h
nctime.obj: nctime.c
rec.obj: ..\libsrc\netcdf.h
rec.obj: emalloc.h
rec.obj: error.h
rec.obj: rec.c
rec.obj: testcdf.h
rec.obj: tests.h
rec.obj: val.h
slabs.obj: ..\libsrc\netcdf.h
slabs.obj: add.h
slabs.obj: emalloc.h
slabs.obj: error.h
slabs.obj: slabs.c
slabs.obj: testcdf.h
slabs.obj: tests.h
val.obj: ..\libsrc\netcdf.h
val.obj: emalloc.h
val.obj: error.h
val.obj: testcdf.h
val.obj: val.c
val.obj: val.h
vardef.obj: ..\libsrc\netcdf.h
vardef.obj: add.h
vardef.obj: emalloc.h
vardef.obj: error.h
vardef.obj: testcdf.h
vardef.obj: tests.h
vardef.obj: vardef.c
varget.obj: ..\libsrc\netcdf.h
varget.obj: emalloc.h
varget.obj: error.h
varget.obj: testcdf.h
varget.obj: tests.h
varget.obj: varget.c
vargetg.obj: ..\libsrc\netcdf.h
vargetg.obj: emalloc.h
vargetg.obj: error.h
vargetg.obj: testcdf.h
vargetg.obj: tests.h
vargetg.obj: vargetg.c
varput.obj: ..\libsrc\netcdf.h
varput.obj: emalloc.h
varput.obj: error.h
varput.obj: testcdf.h
varput.obj: tests.h
varput.obj: val.h
varput.obj: varput.c
varputg.obj: ..\libsrc\netcdf.h
varputg.obj: emalloc.h
varputg.obj: error.h
varputg.obj: testcdf.h
varputg.obj: tests.h
varputg.obj: val.h
varputg.obj: varputg.c
vartests.obj: ..\libsrc\netcdf.h
vartests.obj: add.h
vartests.obj: emalloc.h
vartests.obj: error.h
vartests.obj: testcdf.h
vartests.obj: tests.h
vartests.obj: vartests.c
vputget.obj: ..\libsrc\netcdf.h
vputget.obj: add.h
vputget.obj: emalloc.h
vputget.obj: error.h
vputget.obj: testcdf.h
vputget.obj: tests.h
vputget.obj: val.h
vputget.obj: vputget.c
vputgetg.obj: ..\libsrc\netcdf.h
vputgetg.obj: add.h
vputgetg.obj: emalloc.h
vputgetg.obj: error.h
vputgetg.obj: testcdf.h
vputgetg.obj: tests.h
vputgetg.obj: val.h
vputgetg.obj: vputgetg.c
