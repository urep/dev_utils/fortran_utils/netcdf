# nmake Makefile for ncdump(1).
#
# $Id: Makefile,v 1.9 1997/05/15 20:11:47 steve Exp $

!IF "$(CFG)" == ""
CFG=Release
!ENDIF 
!IF "$(CFG)" != "Debug" && "$(CFG)" != "Release"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "msoft.mak" CFG="Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Release"
!MESSAGE "Debug"
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF  "$(CFG)" == "Debug"
COPT=  /nologo /FD /MTd /Gm /Zi /Od /D "_DEBUG"
!ELSE 
# Release
COPT= /nologo  /FD /MT          /O2
!ENDIF 

INCLUDES= /I..\libsrc
DEFINES= /D "NDEBUG" /D "DLL_NETCDF"
CFLAGS= $(COPT) $(INCLUDES) $(DEFINES)

PATH = ..\libsrc;../ncgen;$(PATH)

SRCS		= ncdump.c vardata.c dumplib.c getopt.c

OBJS		= $(SRCS:.c=.obj)

LIBRARY		= ..\libsrc\netcdf.lib

all:		ncdump.exe

test:	ncdump.exe
	ncgen.exe -o test0.nc -n test0.cdl
	.\ncdump.exe test0.nc > test1.cdl
	ncgen.exe -o test1.nc -n test1.cdl
	.\ncdump -n test0 test1.nc > test2.cdl
	diff test1.cdl test2.cdl && \
	     echo "*** $(PROGRAM) test successful ***"

install: ncdump.exe
	copy ncdump.exe $(BINDIR)

ncdump.exe: $(OBJS) $(LIBRARY)
	$(CC) $(CFLAGS) $(OBJS) $(LIBRARY) /Fe$@

clean:
	 -@erase *.obj test0.nc test1.nc test1.cdl test2.cdl > NUL 2>&1

distclean: clean
	-@erase ncdump.exe *.pdb *.ilk *.idb *.rsp > NUL 2>&1

#depend
dumplib.obj: ..\libsrc\netcdf.h
dumplib.obj: dumplib.c
dumplib.obj: dumplib.h
ncdump.obj: ..\libsrc\netcdf.h
ncdump.obj: dumplib.h
ncdump.obj: ncdump.c
ncdump.obj: ncdump.h
ncdump.obj: vardata.h
vardata.obj: ..\libsrc\netcdf.h
vardata.obj: dumplib.h
vardata.obj: ncdump.h
vardata.obj: vardata.c
vardata.obj: vardata.h
