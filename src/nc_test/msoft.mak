# nmake Makefile for netCDF (semi)exhaustive test.
#
# $Id: Makefile,v 1.10 1997/05/15 20:12:26 steve Exp $

# Release
# COPT= /nologo /W1 /FD /MT          /O2
# /W3 generates many warnings, all of which should be okay...
COPT=   /nologo /W1 /FD /MTd /Gm /Zi /Od /D "_DEBUG"
INCLUDES= /I..\libsrc
DEFINES= /D "NDEBUG" /D "DLL_NETCDF"
CFLAGS= $(COPT) $(INCLUDES) $(DEFINES)

PATH = ..\libsrc;$(PATH)

SRCS		=   nc_test.c \
		    error.c \
		    test_get.c \
		    test_put.c \
		    test_read.c \
		    test_write.c \
		    util.c \
		    getopt.c

OBJS		= $(SRCS:.c=.obj)

LIBRARY		= ..\libsrc\netcdf.lib

all:		nc_test.exe

test:		nc_test.exe test.nc
	.\nc_test.exe

readonly:	nc_test.exe test.nc
	.\nc_test -r

test.nc:  nc_test.exe
	.\nc_test -c

install:

nc_test.exe: $(OBJS) $(LIBRARY)
	$(CC) $(CFLAGS) $(OBJS) $(LIBRARY) /Fe$@

clean:
	-@erase *.obj nc_test.exe test.nc > NUL 2>&1
	-@erase *.pdb *.ilk *.idb *.rsp > NUL 2>&1

distclean: clean

#depend
error.obj: error.c
nc_test.obj: ..\libsrc\netcdf.h
nc_test.obj: error.h
nc_test.obj: nc_test.c
nc_test.obj: tests.h
test_get.obj: ..\libsrc\netcdf.h
test_get.obj: error.h
test_get.obj: test_get.c
test_get.obj: tests.h
test_put.obj: ..\libsrc\netcdf.h
test_put.obj: error.h
test_put.obj: test_put.c
test_put.obj: tests.h
test_read.obj: ..\libsrc\netcdf.h
test_read.obj: error.h
test_read.obj: test_read.c
test_read.obj: tests.h
test_write.obj: ..\libsrc\netcdf.h
test_write.obj: error.h
test_write.obj: test_write.c
test_write.obj: tests.h
util.obj: ..\libsrc\netcdf.h
util.obj: error.h
util.obj: tests.h
util.obj: util.c
