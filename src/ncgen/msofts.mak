#
# nmake Makefile for netcdf ncgen(1) using static library
# JCaron 3/18/98
#

!IF "$(CFG)" == ""
CFG=Release
!ENDIF 
!IF "$(CFG)" != "Debug" && "$(CFG)" != "Release"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "msoft.mak" CFG="Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Release"
!MESSAGE "Debug"
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF  "$(CFG)" == "Debug"
COPT= /nologo /MTd /W1 /Gm /GX /Zi /Od /YX 
DEFINES=  /D "_DEBUG" 
!ELSE 
# Release
COPT= /nologo /MT /W1 /GX /O2 /YX
DEFINES =  /D "NDEBUG" 
!ENDIF 

INCLUDES= /I..\libsrc
DEFINES= $(DEFINES) /D "WIN32" /D "_WINDOWS" /D "YY_NEVER_INTERACTIVE"
CFLAGS= $(COPT) $(INCLUDES) $(DEFINES)

FOR=df
FFLAGS= /nologo /Zi /Od

PATH = ..\ncdump;$(PATH)

SRCS		= main.c load.c ncgentab.c escapes.c \
		  getfill.c init.c genlib.c getopt.c

OBJS		= $(SRCS:.c=.obj)

LIBRARY		= ..\libsrc\netcdfs.lib
LINKFLAGS 	= /NODEFAULTLIB:LIBCMT

all:		ncgen.exe

test:	ncgen.exe b-test c-test f-test

install: ncgen.exe
	copy ncgen.exe $(BINDIR)

ncgen.exe: $(OBJS) $(LIBRARY)
	$(CC) $(CFLAGS) $(OBJS) $(LIBRARY)  /Fe$@

#
# test "-b" option of ncgen
#
b-test:		ncgen.exe c1.cdl
	.\ncgen.exe -b c1.cdl
	ncdump c1.nc > c2.cdl
	diff c1.cdl c2.cdl

#
# test "-c" option of ncgen
#
c-test:	ctest.exe
	.\ctest
	ncdump -n c1 ctest0.nc > ctest1.cdl
	diff c1.cdl ctest1.cdl

ctest.exe: ctest.obj
	$(CC) /nologo ctest.obj $(LIBRARY)  /o $@ /link $(LINKFLAGS)

ctest.obj: ctest.c ..\libsrc\netcdf.h
	$(CC) /nologo /c $(INCLUDES) ctest.c

ctest.c:  ncgen.exe c1.cdl
	.\ncgen.exe -c -o ctest0.nc c0.cdl > $@



c1.cdl:	ncgen.exe c0.cdl
	.\ncgen.exe -b -o c0.nc c0.cdl
	ncdump -n c1 c0.nc > $@

#
# test "-f" option of ncgen
#
f-test: ftest.exe
	.\ftest
	ncdump -n c1 ftest0.nc > ftest1.cdl
	diff c1.cdl ftest1.cdl

ftest.exe: ftest.obj
	$(FOR) $(FFLAGS) ftest.obj \
		 /link $(LIBRARY)  $(LINKFLAGS) /NODEFAULTLIB:LIBC /out:$@

ftest.obj: ftest.for
	$(FOR) /c $(FFLAGS) /I..\fortran $*.for

ftest.for: ftest.f
	$(CC) /nologo /EP /C $(INCLUDES) /I..\fortran $(DEFINES) $? > $@

ftest.f: ncgen.exe c0.cdl
	.\ncgen.exe -f -o ftest0.nc c0.cdl > ftest.f


clean:
	 -@erase *.obj *.for test0.nc test1.nc test1.cdl test2.cdl > NUL 2>&1

testclean:
	-@erase netcdf.inc \
		  c0.nc c1.cdl c1.nc c2.cdl \
		  f0.nc \
		  ctest.c ctest.obj ctest.exe ctest0.nc ctest1.cdl \
		  ftest.f ftest.obj ftest.exe ftest0.nc ftest1.cdl > NUL 2>&1

distclean: clean testclean
	-@erase ncgen.exe *.pdb *.ilk *.idb *.rsp > NUL 2>&1

#depend
close.obj: ..\libsrc\netcdf.h
close.obj: close.c
close.obj: generic.h
close.obj: genlib.h
close.obj: ncgen.h
escapes.obj: ..\libsrc\netcdf.h
escapes.obj: escapes.c
escapes.obj: generic.h
escapes.obj: genlib.h
escapes.obj: ncgen.h
genlib.obj: ..\libsrc\netcdf.h
genlib.obj: generic.h
genlib.obj: genlib.c
genlib.obj: genlib.h
genlib.obj: ncgen.h
getfill.obj: ..\libsrc\netcdf.h
getfill.obj: generic.h
getfill.obj: genlib.h
getfill.obj: getfill.c
getfill.obj: ncgen.h
init.obj: ..\libsrc\netcdf.h
init.obj: generic.h
init.obj: init.c
init.obj: ncgen.h
load.obj: ..\libsrc\netcdf.h
load.obj: generic.h
load.obj: genlib.h
load.obj: load.c
load.obj: ncgen.h
main.obj: ..\libsrc\netcdf.h
main.obj: generic.h
main.obj: genlib.h
main.obj: main.c
main.obj: ncgen.h
