#
# nmake Makefile for netcdf libsrc static library
# JCaron 3/18/98
#

!IF "$(CFG)" == ""
CFG=Release
!ENDIF 

!IF "$(CFG)" != "Debug" && "$(CFG)" != "Release"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "msoft.mak" CFG="Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Release"
!MESSAGE "Debug"
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF  "$(CFG)" == "Debug"
COPT= /nologo /MTd /W3 /Gm /GX /Zi /Od /YX  /D "_DEBUG" 
!ELSE 
COPT= /nologo /MT /W3 /GX /O2 /YX  /D "NDEBUG" 
!ENDIF 

DEFINES = /D "WIN32" /D "_WINDOWS" /D "VERSION"="$(Version)"

CFLAGS= $(COPT) $(DEFINES) 

LIBRARY	= netcdfs.lib
HEADER = netcdf.h

LIB_CSRCS = \
	attr.c \
	dim.c \
	error.c \
	libvers.c \
	nc.c \
	posixio.c \
	ncx.c \
	putget.c \
	string.c \
	v1hpg.c \
	v2i.c \
	var.c

LIB_OBJS = $(LIB_CSRCS:.c=.obj)

GARBAGE	= t_ncio.obj t_ncio.exe t_ncx.obj t_ncx.exe t_ncxx.obj t_ncxx.exe \
	t_nc.obj t_nc.exe test.nc


all: $(LIBRARY)

check: test

install: $(INCDIR)/$(HEADER) \
	$(LIBDIR)/$(LIBRARY) \

LINK=link.exe
LIBMAKE=lib.exe
$(LIBRARY): $(LIB_OBJS)
    $(LIBMAKE) /out:$(LIBRARY) $(LIB_OBJS)

test:	t_nc.exe
	t_nc
	cmp test.nc test_nc.sav
	@echo '*** Success ***'

t_nc.exe: t_nc.obj $(LIBRARY)
	$(CC) $(CFLAGS) t_nc.obj $(LIBRARY) /Fe$@

t_nc.obj: t_nc.c netcdf.h
	$(CC) $(CFLAGS) /c t_nc.c

attr.c:	attr.m4
ncx.c:	ncx.m4
putget.c: putget.m4
t_ncxx.c: t_ncxx.m4

clean:
	-@erase *.obj t_nc.exe test.nc > NUL 2>&1

distclean: clean
	-@erase $(GARBAGE) > NUL 2>&1
	-@erase *.pdb *.ilk *.idb *.rsp > NUL 2>&1
	-@erase *.dll *.lib > NUL 2>&1

	
	
# depends
attr.obj: attr.c
attr.obj: fbits.h
attr.obj: nc.h
attr.obj: ncconfig.h
attr.obj: ncio.h
attr.obj: ncx.h
attr.obj: netcdf.h
attr.obj: rnd.h
dim.obj: dim.c
dim.obj: fbits.h
dim.obj: nc.h
dim.obj: ncconfig.h
dim.obj: ncio.h
dim.obj: ncx.h
dim.obj: netcdf.h
dim.obj: rnd.h
error.obj: error.c
error.obj: netcdf.h
ffio.obj: fbits.h
ffio.obj: ffio.c
ffio.obj: ncconfig.h
ffio.obj: ncio.h
ffio.obj: netcdf.h
ffio.obj: rnd.h
libvers.obj: libvers.c
libvers.obj: netcdf.h
n.obj: n.c
nc.obj: fbits.h
nc.obj: nc.c
nc.obj: nc.h
nc.obj: ncconfig.h
nc.obj: ncio.h
nc.obj: ncx.h
nc.obj: netcdf.h
nc.obj: rnd.h
ncio.obj: fbits.h
ncio.obj: ncconfig.h
ncio.obj: ncio.c
ncio.obj: ncio.h
ncio.obj: netcdf.h
ncio.obj: posixio.c
ncio.obj: rnd.h
ncx.obj: ncconfig.h
ncx.obj: ncx.c
ncx.obj: ncx.h
ncx.obj: rnd.h
ncx_cray.obj: ncx_cray.c
ncxx.obj: ncxx.c
posixio.obj: fbits.h
posixio.obj: ncconfig.h
posixio.obj: ncio.h
posixio.obj: netcdf.h
posixio.obj: posixio.c
posixio.obj: rnd.h
putget.obj: fbits.h
putget.obj: nc.h
putget.obj: ncconfig.h
putget.obj: ncio.h
putget.obj: ncx.h
putget.obj: netcdf.h
putget.obj: putget.c
putget.obj: rnd.h
string.obj: fbits.h
string.obj: nc.h
string.obj: ncconfig.h
string.obj: ncio.h
string.obj: ncx.h
string.obj: netcdf.h
string.obj: rnd.h
string.obj: string.c
t_ncio.obj: ncconfig.h
t_ncio.obj: ncio.h
t_ncio.obj: ncx.h
t_ncio.obj: netcdf.h
t_ncio.obj: rnd.h
t_ncio.obj: t_ncio.c
t_ncx.obj: ncconfig.h
t_ncx.obj: ncx.h
t_ncx.obj: rnd.h
t_ncx.obj: t_ncx.c
t_ncxx.obj: ncconfig.h
t_ncxx.obj: ncx.h
t_ncxx.obj: rnd.h
t_ncxx.obj: t_ncxx.c
tf.obj: netcdf.h
tf.obj: tf.c
ti.obj: netcdf.h
ti.obj: ti.c
tvf.obj: ncconfig.h
tvf.obj: ncx.h
tvf.obj: rnd.h
tvf.obj: tvf.c
v1hpg.obj: fbits.h
v1hpg.obj: nc.h
v1hpg.obj: ncconfig.h
v1hpg.obj: ncio.h
v1hpg.obj: ncx.h
v1hpg.obj: netcdf.h
v1hpg.obj: rnd.h
v1hpg.obj: v1hpg.c
v2i.obj: fbits.h
v2i.obj: nc.h
v2i.obj: ncconfig.h
v2i.obj: ncio.h
v2i.obj: netcdf.h
v2i.obj: v2i.c
var.obj: fbits.h
var.obj: nc.h
var.obj: ncconfig.h
var.obj: ncio.h
var.obj: ncx.h
var.obj: netcdf.h
var.obj: rnd.h
var.obj: var.c
