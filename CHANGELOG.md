# Change Log

## [3.5.1](https://forgemia.inra.fr/urep/dev_utils/fortran_utils/netcdf/-/releases/3.5.1) (2022-06-07)

- See full changelog [here](https://docs.unidata.ucar.edu/netcdf-c/current/RELEASE_NOTES.html)
- First version packaged by UREP Gitlab project
