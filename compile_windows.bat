@echo off

set LOG_FILE="%~dp0compile_windows.log"

:: Set Intel Fortran environment
call "C:\Program Files (x86)\Intel\oneAPI\setvars.bat" intel64 vs2022

:: Call sub script for compilation
call "%~dp0compile_windows_sub.bat" > %LOG_FILE% 2>&1

echo **************************
echo *** end (ERRORLEVEL=%ERRORLEVEL%) ***
echo **************************

pause
