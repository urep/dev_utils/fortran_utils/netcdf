# NetCDF

[![pipeline status](https://forgemia.inra.fr/urep/dev_utils/fortran_utils/netcdf/badges/main/pipeline.svg)](https://forgemia.inra.fr/urep/dev_utils/fortran_utils/netcdf/-/commits/main)
[![Latest Release](https://forgemia.inra.fr/urep/dev_utils/fortran_utils/netcdf/-/badges/release.svg)](https://forgemia.inra.fr/urep/dev_utils/fortran_utils/netcdf/-/releases)

Library to manage in Fortran machine-independent data formats that support the creation, access, and sharing of array-oriented scientific data.

This project host code from versions of NetCDF lib used in INRAE UREP.  
[The original project page](https://www.unidata.ucar.edu/software/netcdf/)

## Getting started

### Get precompiled packages

This project's CI create compiled packages that are host in https://forgemia.inra.fr/urep/dev_utils/fortran_utils/netcdf/-/packages.

### Compile on Windows

Use instructions in `src/WIN32_README.TXT` file for explanations. For static libray see section 2.1.

These instructions are scripted in the `compile_windows.bat` file. Compilation results are copied in `include`, `lib` and `bin` directories.

prerequisites:

- Microsoft Visual Studio 2022 (NMake & MSVC C++ compiler)
- [Intel Fortran Compiler Classic](https://www.intel.com/content/www/us/en/developer/articles/tool/oneapi-standalone-components.html#fortran)

### Compile on Linux

Use instructions in `src/INSTALL.html` file for explanations.

These instructions are scripted in the `compile_linux.sh` file. Compilation results are copied in `include`, `lib` and `bin` directories.

prerequisites:

- `sudo apt-get install build-essential`
- [Intel Fortran Compiler Classic](https://www.intel.com/content/www/us/en/developer/articles/tool/oneapi-standalone-components.html#fortran)

## License

This project doesn't change the NetCDF lilbrary, so the license is the same, see [COPYRIGHT file](src/COPYRIGHT)
