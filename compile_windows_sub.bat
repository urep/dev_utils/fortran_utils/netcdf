set MAKEFILE_FILE_NAME=msofts.mak
:: Use this line instead for dynamic linking (aka dll)
::set MAKEFILE_FILE_NAME=msoft.mak

:: Copy files that are auto-generated on linux
copy /Y "%~dp0src\libsrc\ncconfig.h.win" "%~dp0src\libsrc\ncconfig.h"
copy /Y "%~dp0src\fortran\nfconfig.inc.win" "%~dp0src\fortran\nfconfig.inc"

cd "%~dp0src\libsrc"
nmake clean /f %MAKEFILE_FILE_NAME%
echo ***********************************
echo *** libsrc clean (ERRORLEVEL=%ERRORLEVEL%) ***
echo ***********************************
If %ERRORLEVEL% NEQ 0 (
  Exit /B %ERRORLEVEL%
)
:: This will build static netcdfs.lib
nmake /f %MAKEFILE_FILE_NAME%
echo *************************************
echo *** libsrc compile (ERRORLEVEL=%ERRORLEVEL%) ***
echo *************************************
If %ERRORLEVEL% NEQ 0 (
  Exit /B %ERRORLEVEL%
)
:: (optional) This will make and run the simple test
REM nmake /f %MAKEFILE_FILE_NAME% test
REM echo **********************************
REM echo *** libsrc test (ERRORLEVEL=%ERRORLEVEL%) ***
REM echo **********************************
REM If %ERRORLEVEL% NEQ 0 (
  REM Exit /B %ERRORLEVEL%
REM )

cd "%~dp0src\fortran"
nmake clean /f %MAKEFILE_FILE_NAME%
echo ************************************
echo *** fortran clean (ERRORLEVEL=%ERRORLEVEL%) ***
echo ************************************
If %ERRORLEVEL% NEQ 0 (
  Exit /B %ERRORLEVEL%
)
:: This will build the fortran interface in src\libsrc
nmake /f %MAKEFILE_FILE_NAME%
echo **************************************
echo *** fortran compile (ERRORLEVEL=%ERRORLEVEL%) ***
echo **************************************
If %ERRORLEVEL% NEQ 0 (
  Exit /B %ERRORLEVEL%
)
REM :: (optional) This tests the netcdf-2 fortran interface
REM nmake /f %MAKEFILE_FILE_NAME% test
REM echo ***********************************
REM echo *** fortran test (ERRORLEVEL=%ERRORLEVEL%) ***
REM echo ***********************************
REM If %ERRORLEVEL% NEQ 0 (
  REM Exit /B %ERRORLEVEL%
REM )

REM cd "%~dp0src\nctest"
REM :: (optional, but recommended) This tests the netcdf-2 C interface
REM nmake /f %MAKEFILE_FILE_NAME% test
REM echo ***************************************
REM echo *** C-interface test (ERRORLEVEL=%ERRORLEVEL%) ***
REM echo ***************************************
REM If %ERRORLEVEL% NEQ 0 (
  REM Exit /B %ERRORLEVEL%
REM )

REM cd "%~dp0src\nc_test"
REM :: (optional, but highly recommended) This tortures the netcdf-3 C interface
REM nmake /f %MAKEFILE_FILE_NAME% test
REM echo ****************************************
REM echo *** C-interface test 2 (ERRORLEVEL=%ERRORLEVEL%) ***
REM echo ****************************************
REM If %ERRORLEVEL% NEQ 0 (
  REM Exit /B %ERRORLEVEL%
REM )

REM cd "%~dp0src\nf_test"
REM :: (optional, but highly recommended if you built the fortran interface)
REM :: This tortures the netcdf-3 fortran interface
REM nmake /f %MAKEFILE_FILE_NAME% test
REM echo *********************************************
REM echo *** fortran interface test (ERRORLEVEL=%ERRORLEVEL%) ***
REM echo *********************************************
REM If %ERRORLEVEL% NEQ 0 (
  REM Exit /B %ERRORLEVEL%
REM )

cd "%~dp0src\ncdump"
nmake clean /f %MAKEFILE_FILE_NAME%
echo ***********************************
echo *** ncdump clean (ERRORLEVEL=%ERRORLEVEL%) ***
echo ***********************************
If %ERRORLEVEL% NEQ 0 (
  Exit /B %ERRORLEVEL%
)
:: (optional) This makes ncdump.exe
nmake /f %MAKEFILE_FILE_NAME%
echo *************************************
echo *** ncdump compile (ERRORLEVEL=%ERRORLEVEL%) ***
echo *************************************
If %ERRORLEVEL% NEQ 0 (
  Exit /B %ERRORLEVEL%
)

cd "%~dp0src\ncgen"
nmake clean /f %MAKEFILE_FILE_NAME%
echo **********************************
echo *** ncgen clean (ERRORLEVEL=%ERRORLEVEL%) ***
echo **********************************
If %ERRORLEVEL% NEQ 0 (
  Exit /B %ERRORLEVEL%
)
:: (optional) This makes ncgen.exe
nmake /f %MAKEFILE_FILE_NAME%
echo ************************************
echo *** ncgen compile (ERRORLEVEL=%ERRORLEVEL%) ***
echo ************************************
If %ERRORLEVEL% NEQ 0 (
  Exit /B %ERRORLEVEL%
)

REM cd "%~dp0src\ncdump"
REM :: (optional) This tests ncdump
REM nmake /f %MAKEFILE_FILE_NAME% test
REM echo **********************************
REM echo *** ncdump test (ERRORLEVEL=%ERRORLEVEL%) ***
REM echo **********************************
REM If %ERRORLEVEL% NEQ 0 (
  REM Exit /B %ERRORLEVEL%
REM )

REM cd "%~dp0src\ncgen"
REM :: (optional) This tests ncgen
REM nmake /f %MAKEFILE_FILE_NAME% test
REM echo *********************************
REM echo *** ncgen test (ERRORLEVEL=%ERRORLEVEL%) ***
REM echo *********************************
REM If %ERRORLEVEL% NEQ 0 (
  REM Exit /B %ERRORLEVEL%
REM )

if not exist "%~dp0include\" mkdir "%~dp0include\"
if not exist "%~dp0lib\" mkdir "%~dp0lib\"
if not exist "%~dp0bin\" mkdir "%~dp0bin\"

if exist "%~dp0src\libsrc\netcdf.h"    copy /Y "%~dp0src\libsrc\netcdf.h" "%~dp0include\"
if exist "%~dp0src\fortran\netcdf.inc" copy /Y "%~dp0src\fortran\netcdf.inc" "%~dp0include\"
if exist "%~dp0src\libsrc\netcdf.lib"  copy /Y "%~dp0src\libsrc\netcdf.lib" "%~dp0lib\"
if exist "%~dp0src\libsrc\netcdfs.lib" copy /Y "%~dp0src\libsrc\netcdfs.lib" "%~dp0lib\"
if exist "%~dp0src\libsrc\netcdf.dll"  copy /Y "%~dp0src\libsrc\netcdf.dll" "%~dp0bin\"
if exist "%~dp0src\ncdump\ncdump.exe"  copy /Y "%~dp0src\ncdump\ncdump.exe" "%~dp0bin\"
if exist "%~dp0src\ncgen\ncgen.exe"    copy /Y "%~dp0src\ncgen\ncgen.exe" "%~dp0bin\"
