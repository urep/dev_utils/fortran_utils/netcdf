include:
  - project: 'urep/dev_utils/gitlab-ci-templates'
    file:
      - '/templates/templates-gitlab-package-registry.yml'

.base-linux:
  image: registry.forgemia.inra.fr/urep/dev_utils/fortran_utils/docker-fortran-intel-compiler:v1.0
  before_script:
    #- export CPPFLAGS=-Df2cFortran
    - export CPPFLAGS=-DNAGf90Fortran
    - export CXX=
    - cd src
    - ./configure --disable-netcdf-4 --disable-cxx --disable-shared --prefix=$CI_PROJECT_DIR
  tags:
    - docker

build-linux:
  extends:
    - .base-linux
  script:
    - make
  artifacts:
    untracked: true

check-linux:
  extends:
    - .base-linux
  needs: ["build-linux"]
  script:
    - make check
  artifacts:
    untracked: true

install-linux:
  extends:
    - .base-linux
  needs: ["check-linux"]
  rules:
    # Run only for tags
    - if: $CI_COMMIT_TAG
      when: on_success
  script:
    # deploy useful files in include and lib folders
    - make install
  artifacts:
    untracked: true

.base-windows:
  variables:
    MAKEFILE_FILE_NAME: msofts.mak
    # Use this line instead for dynamic linking (aka dll)
    #MAKEFILE_FILE_NAME: msoft.mak
  before_script:
    # Set Intel Fortran environment
    - 'call "C:\Program Files (x86)\Intel\oneAPI\setvars.bat" intel64 vs2022'
    # Copy files that are auto-generated on linux
    - copy /Y "%CI_PROJECT_DIR%\src\libsrc\ncconfig.h.win" "%CI_PROJECT_DIR%\src\libsrc\ncconfig.h"
    - copy /Y "%CI_PROJECT_DIR%\src\fortran\nfconfig.inc.win" "%CI_PROJECT_DIR%\src\fortran\nfconfig.inc"
  tags:
    - windows-cmd-shell

build-windows:
  extends:
    - .base-windows
  script:
    - cd %CI_PROJECT_DIR%\src\libsrc
    # This will build static netcdfs.lib
    - nmake /f %MAKEFILE_FILE_NAME%
    - cd %CI_PROJECT_DIR%\src\fortran
    # This will build the fortran interface in src\libsrc
    - nmake /f %MAKEFILE_FILE_NAME%
    - cd %CI_PROJECT_DIR%\src\ncdump
    # (optional) This makes ncdump.exe
    - nmake /f %MAKEFILE_FILE_NAME%
    # (optional) This makes ncgen.exe
    - nmake /f %MAKEFILE_FILE_NAME%
  artifacts:
    untracked: true

check-windows:
  extends:
    - .base-windows
  needs: ["build-windows"]
  script:
    # Tests are not working on Windows :(
    - cd %CI_PROJECT_DIR%\src\libsrc
    # (optional) This will make and run the simple test
    #- nmake /f %MAKEFILE_FILE_NAME% test
    - cd %CI_PROJECT_DIR%\src\fortran
    # (optional) This tests the netcdf-2 fortran interface
    # - nmake /f %MAKEFILE_FILE_NAME% test
    - cd %CI_PROJECT_DIR%\src\nctest
    # (optional, but recommended) This tests the netcdf-2 C interface
    # - nmake /f %MAKEFILE_FILE_NAME% test
    - cd %CI_PROJECT_DIR%\src\nc_test
    # (optional, but highly recommended) This tortures the netcdf-3 C interface
    # - nmake /f %MAKEFILE_FILE_NAME% test
    - cd %CI_PROJECT_DIR%\src\nf_test
    # (optional, but highly recommended if you built the fortran interface)
    # This tortures the netcdf-3 fortran interface
    # - nmake /f %MAKEFILE_FILE_NAME% test
    - cd %CI_PROJECT_DIR%\src\ncdump
    # (optional) This tests ncdump
    # - nmake /f %MAKEFILE_FILE_NAME% test
    - cd %CI_PROJECT_DIR%\src\ncgen
    # (optional) This tests ncgen
    # - nmake /f %MAKEFILE_FILE_NAME% test
  artifacts:
    untracked: true

install-windows:
  tags:
    - windows-cmd-shell
  needs: ["check-windows"]
  rules:
    # Run only for tags
    - if: $CI_COMMIT_TAG
      when: on_success
  script:
    # deploy useful files in include, bin and lib folders
    - if not exist "include\" mkdir "include\"
    - if not exist "lib\" mkdir "lib\"
    - if not exist "bin\" mkdir "bin\"
    - if exist "src\libsrc\netcdf.h"    copy /Y "src\libsrc\netcdf.h" "include\"
    - if exist "src\fortran\netcdf.inc" copy /Y "src\fortran\netcdf.inc" "include\"
    - if exist "src\libsrc\netcdf.lib"  copy /Y "src\libsrc\netcdf.lib" "lib\"
    - if exist "src\libsrc\netcdfs.lib" copy /Y "src\libsrc\netcdfs.lib" "lib\"
    - if exist "src\libsrc\netcdf.dll"  copy /Y "src\libsrc\netcdf.dll" "bin\"
    - if exist "src\ncdump\ncdump.exe"  copy /Y "src\ncdump\ncdump.exe" "bin\"
    - if exist "src\ncgen\ncgen.exe"    copy /Y "src\ncgen\ncgen.exe" "bin\"
  artifacts:
    untracked: true

.base-deploy:
  extends:
    - .gitlab-generic-package-upload
  variables:
    PACKAGE_NAME: 'NetCDF'
    FILENAME: ${PACKAGE_NAME}-${GITLAB_PACKAGE_VERSION}.tar.gz
  before_script:
    # compress useful files
    - tar -czvf ${FILENAME} bin/ include/ lib/

deploy-linux:
  extends:
    - .base-deploy
  variables:
    GITLAB_PACKAGE_VERSION: ${CI_COMMIT_TAG}-linux
  needs: ["install-linux"]

deploy-windows:
  extends:
    - .base-deploy
  variables:
    GITLAB_PACKAGE_VERSION: ${CI_COMMIT_TAG}-windows
  needs: ["install-windows"]
