#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# Set env var for Intel Fortran compiler
source ~/intel/oneapi/setvars.sh

export CC=gcc
export FC=ifort
export F70=ifort
export F90=ifort

cd src

make clean

./configure --disable-netcdf-4 --disable-cxx --disable-shared --prefix=$SCRIPT_DIR

echo "***************************"
echo "*** Configure (error=$?) ***"
echo "***************************"
if [ $? -ne 0 ]
then
  exit $?
fi

make

echo "**********************"
echo "*** Make (error=$?) ***"
echo "**********************"
if [ $? -ne 0 ]
then
  exit $?
fi

make check

echo "***********************"
echo "*** Check (error=$?) ***"
echo "***********************"
if [ $? -ne 0 ]
then
  exit $?
fi

make install

echo "*************************"
echo "*** Install (error=$?) ***"
echo "*************************"
if [ $? -ne 0 ]
then
  exit $?
fi

